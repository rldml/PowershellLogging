class LogModule{
    
    <# Base-Class #>

    <# DataStructure #>
    [System.String] $Name;

    LogModule(){
        $type = $this.GetType();
        if ($type -eq [LogModule]){
            throw "Interface Error. May not instantiate base class.";
        }
    }

    WriteMessage([string]$Message, [string]$Type, [int]$Progress){
        throw "Interface Error. Method must be overrided";
    }

    FinalizeLog(){
        throw "Interface Error. Method must be overrided";
    }

}

class LMMemory : LogModule{
    
    [PSObject[]]$logdata;<#Array to store LogMessages#>
    
    LMMemory([System.String]$Name){
        $this.logdata = @();
        $this.Name = $Name;
    }

    [void]WriteMessage([string]$Message, [string]$Type, [int]$Progress){
        $MessageObject = New-Object -TypeName PSObject;
        Add-Member -InputObject $MessageObject -NotePropertyName "TimeStamp" -NotePropertyValue (Get-Date -Format "yyyy-MM-ddTHH:mm:ss");
        Add-Member -InputObject $MessageObject -NotePropertyName "Message" -NotePropertyValue $Message;
        Add-Member -InputObject $MessageObject -NotePropertyName "Type" -NotePropertyValue $Type;
        Add-Member -InputObject $MessageObject -NotePropertyName "Progress" -NotePropertyValue $Progress;
        $this.logdata += $MessageObject;
    }

    [void]FinalizeLog(){
        <# do nothing #>
    }
}

class LMVerbose : LogModule{
    
    LMVerbose([System.String]$Name){
        $this.Name = $Name;
    }

    [void]WriteMessage([string]$Message, [string]$Type, [int]$Progress){
        $ts = Get-Date -Format "yyyy-MM-ddTHH:mm:ss";
        $msg = $ts + " " + $Progress.ToString("00") + " " + $Type + " " + $Message;
        Write-Verbose $msg;
    }

    [void]FinalizeLog(){
        <#do nothing#>
    }
}

class LMFile : LogModule{
    
    <#Datastructure#>
    [System.IO.DirectoryInfo]$BasePath; <# BasePath to store LogFiles #>
    [System.Int32]$MaxFiles; <# Max Numbers of Files #>
    [System.Int64]$MaxSize; <# Max Size of Logfile in Bytes #>
    [System.Boolean]$FileRotation; <# Enables FileRotation #>

    <# Methods #>

    LMFile([System.String]$Name, [System.String]$BasePath, [System.Int32]$MaxSize){
        $this.Name = $Name;
        $PathExist = Test-Path $BasePath
        if ($PathExist -eq $false){
            throw "Verzeichnispfad nicht gefunden."
        } else {
            $this.BasePath = Get-Item -Path $BasePath;
        }
        $this.MaxSize = $MaxSize;
        $this.DisableFileRotation();
    }

    [void]EnableFileRotation([System.Int32]$MaxFiles){
        $this.FileRotation = $true;
        $this.MaxFiles = $MaxFiles;
    }

    [void]DisableFileRotation(){
        $this.FileRotation = $false;
        $this.MaxFiles = [System.Int32]::MaxValue;
    }

    [System.Int32]GetLogFileNumber([System.IO.FileInfo]$File){
        if ($File.Name -notmatch "\d\d\d\d-\d\d-\d\d_\d\d\d.log"){
            throw "Filename differs from naming pattern."
        }
        $ret = 0
        $Name = $File.Name
        $retString = $Name.Substring($Name.IndexOf("_") + 1, 3)
        $tmp = [System.Int32]::TryParse($retString, [ref]$ret)
        return $ret;
    }

    [System.IO.FileInfo]FindLogFile(){
        $Oldest = $false;
        $FileFilter = (Get-Date -Format "yyyy-MM-dd_*.") + "log";
        $FileList = Get-ChildItem -Path $this.BasePath -Filter $FileFilter;
        if ($FileList.Count -eq 0){
            $ergFile = $this.CreateLogFile((Get-Date -Format "yyyy-MM-dd_") + "001.log");
        } else
        {
            $ergFile = $null;
            foreach($file in $FileList){
                if ($ergFile -ne $null){
                    if ($file.LastWriteTime -gt $ergFile.LastWriteTime){
                        $ergFile = $file;
                    }
                } else {
                    $ergFile = $file;
                }
            }
            if (($ergFile.Length+255) -gt $this.MaxSize){
                $i = $this.GetLogFileNumber($ergFile);
                if ($this.FileRotation){
                    if (($i+1) -gt $this.MaxFiles){
                        $i = 1;
                    } else
                    {
                        $i = $i + 1;
                    }
                } else {
                    #Just add a number and write a new LogFile
                    $i = $i + 1;
                }
                $ergFile = $this.CreateLogFile((Get-Date -Format "yyyy-MM-dd_") + $i.toString("000") + ".log");
            }
        }
        return $ergFile;
    }

    [System.IO.FileInfo] CreateLogFile([System.String]$Name){
        $ergFile = New-Item -Path $this.BasePath.FullName -Name $Name -Force
        return $ergFile;
    }

    [void] WriteMessage([string]$Message, [string]$Type, [int]$Progress){
        $file = $this.FindLogFile();
        $Line = (Get-Date -Format "yyyy-MM-dd HH:mm:ss") + " " + $Type + " " + $Message;
        Out-File -FilePath $file.FullName -Encoding utf8 -Append -InputObject $Line
    }

    [void] FinalizeLog(){
        <#do nothing#>
    }
}

class LMEventLog : LogModule{

    <# Write one EventlogEntry at Finalize #>

    [System.String[]] $logdata; <# Array to store LogMessages #>
    [System.String]$EventLogName; <# Name of EventLog #>
    [System.String]$SourceName; <# Name of Source #>
    [System.Int32]$EventID; <# EventID #>
    [System.String] $EntryType; <# Type of Entry. There are three possible Values: Information, Warning and Error #>
    [System.String] $RemoteHost; <# Write Eventlog-Entry  at a different host #>

    LMEventLog([System.String]$Name, [System.String]$LogName, [System.String]$Source, [System.Int32]$EventID){
        $this.logdata = @();
        $this.Name = $Name;
        $this.EventLogName = $LogName;
        $this.SourceName = $Source;
        $this.EventID = $EventID;
        $this.EntryType = "Information";
        $this.RemoteHost = $null;
    }

    [void] SetRemoteHost([System.String]$ComputerName){
        $this.RemoteHost = $ComputerName;
    }

    WriteMessage([string]$Message, [string]$Type, [int]$Progress){
        $ts = Get-Date -Format "yyyy-MM-ddTHH:mm:ss";
        $this.logdata += ($ts + " " + $Type + " " + $Message);

        Write-Host ("Before: " + $this.EntryType + ";" + $Type);
        
        if ($this.EntryType -eq "Information" -and ($Type -like "*Warning*" -or $Type -like "*Error*")){
            $this.EntryType = $Type.Trim();
        }

        Write-Host ("After First Check: " + $this.EntryType + ";" + $Type);
        
        if ($this.EntryType -eq "Warning" -and $Type -like "*Error*"){
            $this.EntryType = $Type.Trim();
        }

        Write-Host ("After Second Check: " + $this.EntryType + ";" + $Type);
    }

    FinalizeLog(){
        $MessageString = "";
        foreach($line in $this.logdata){
            $MessageString += $line + "`r`n";
        }
        if ([System.String]::IsNullOrEmpty($this.RemoteHost)){
            Write-EventLog -LogName $this.EventLogName -Source $this.SourceName -EntryType $this.EntryType -Message $MessageString -EventId $this.EventID;
        } else {
            Write-EventLog -LogName $this.EventLogName -Source $this.SourceName -EntryType $this.EntryType -Message $MessageString -EventId $this.EventID -ComputerName $this.RemoteHost;
        }
        $this.logdata = @();
        Write-Host $this.EntryType;
    }
}

function Write-LogMessage{
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$True)][string]$Message,
        [Parameter(Mandatory=$True)][ValidateSet("Information", "Warning", "Error", "ScriptStart", "ScriptEnd")]$Type,
        [Parameter()]$Progress = 0,
        [Parameter(Mandatory=$True,ValueFromPipeline=$true)][LogModule[]]$LogProfile
    )
    begin{
        switch ($Type){
            "Information" { $TypeOut = "Information" }
            "Warning" { $TypeOut =     "Warning    " }
            "Error" { $TypeOut =       "Error      " }
            "ScriptStart" { $TypeOut = "ScriptStart" }
            "ScriptEnd"{ $TypeOut =    "ScriptEnd  " }
        }
    }
    process{
        foreach($lp in $LogProfile){
            $lp.WriteMessage($Message, $TypeOut, $Progress);
        }
    }
    end{}
}

function Publish-LogMessage{
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$True,ValueFromPipeline=$true)][LogModule[]]$LogProfile
    )
    begin{}
    process{
        foreach($lp in $LogProfile){
            $lp.FinalizeLog();
        }
    }
    end{}
}

<#

.SYNOPSIS
Creates a new log profile

.DESCRIPTION
Creates a new log profile to send messages to defined targets

.PARAMETER BasePath
basepath to create log folders and log files. Only available in File mode

.PARAMETER LogName
name of log. Only available in File and Eventlog mode.

.PARAMETER MaxSize
maximum size of log files. Only available in File mode.

.PARAMETER FileRotation
activates file rotation. only available in File mode

.PARAMETER MaxFiles
maximum count of log files. only available in File mode.

.PARAMETER Mode
defines the mode of log profile.

.PARAMETER Source
the name of the eventsource. Only available in EventLog mode

.PARAMETER EventID
The eventid of the event. Only available in EventLog mode

.PARAMETER ComputerName
Name of the remote computer. Only available in EventLog mode

.PARAMETER Name
name of log profile

#>
function New-LogProfile{
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$true)][ValidateSet("File", "Verbose", "Memory", "EventLog")][string]$Mode,
        [Parameter(Mandatory=$true)][string]$Name
    )
    DynamicParam {
        if ($Mode -eq "File") {
            $paramDictionary = New-Object System.Management.Automation.RuntimeDefinedParameterDictionary

            $param = New-Object System.Management.Automation.ParameterAttribute
            $param.Mandatory = $true
            $attributeCollection = new-object System.Collections.ObjectModel.Collection[System.Attribute]
            $attributeCollection.Add($param)
        
            $BasePathParam = New-Object System.Management.Automation.RuntimeDefinedParameter('BasePath', [string], $attributeCollection)
            $paramDictionary.Add('BasePath', $BasePathParam)

            $BasePathParam = New-Object System.Management.Automation.RuntimeDefinedParameter('LogName', [string], $attributeCollection)
            $paramDictionary.Add('LogName', $BasePathParam)

            $BasePathParam = New-Object System.Management.Automation.RuntimeDefinedParameter('MaxSize', [int], $attributeCollection)
            $paramDictionary.Add('MaxSize', $BasePathParam)

            $param.Mandatory = $false
            $attributeCollection = new-object System.Collections.ObjectModel.Collection[System.Attribute]
            $attributeCollection.Add($param)

            $BasePathParam = New-Object System.Management.Automation.RuntimeDefinedParameter('FileRotation', [switch], $attributeCollection)
            $paramDictionary.Add('FileRotation', $BasePathParam)

            $BasePathParam = New-Object System.Management.Automation.RuntimeDefinedParameter('MaxFiles', [int], $attributeCollection)
            $paramDictionary.Add('MaxFiles', $BasePathParam)

            return $paramDictionary
        }
        if ($Mode -eq "EventLog") {
            $paramDictionary = New-Object System.Management.Automation.RuntimeDefinedParameterDictionary

            $param = New-Object System.Management.Automation.ParameterAttribute
            $param.Mandatory = $true
            $attributeCollection = new-object System.Collections.ObjectModel.Collection[System.Attribute]
            $attributeCollection.Add($param)
        
            $ParamEntry = New-Object System.Management.Automation.RuntimeDefinedParameter('LogName', [string], $attributeCollection)
            $paramDictionary.Add('LogName', $ParamEntry)

            $ParamEntry = New-Object System.Management.Automation.RuntimeDefinedParameter('Source', [string], $attributeCollection)
            $paramDictionary.Add('Source', $ParamEntry)

            $ParamEntry = New-Object System.Management.Automation.RuntimeDefinedParameter('EventID', [int], $attributeCollection)
            $paramDictionary.Add('EventID', $ParamEntry)

            $param.Mandatory = $false
            $attributeCollection = new-object System.Collections.ObjectModel.Collection[System.Attribute]
            $attributeCollection.Add($param)

            $ParamEntry = New-Object System.Management.Automation.RuntimeDefinedParameter('ComputerName', [string], $attributeCollection)
            $paramDictionary.Add('ComputerName', $ParamEntry)
            
            return $paramDictionary
        }
    }
    begin{
        if ($Mode -eq "File"){
            if ($PSBoundParameters.MaxSize -lt 10240){
                throw "MaxSize must be at least 10240 bytes"
            }
            if ($PSBoundParameters.BasePath.ToString().Trim() -eq ""){
                throw "Basepath is mandatory"
            }
            if ($PSBoundParameters.FileRotation -eq $true -and $PSBoundParameters.MaxFiles -lt 1){
                throw "Filerotation needs MaxFiles with a value greater than zero."
            }
        }
    }
    process{
        switch($Mode){
            "File"{
                $BasePath = $PSBoundParameters.BasePath
                $LogName = $PSBoundParameters.LogName
                $FileRotation = $PSBoundParameters.FileRotation
                $MaxSize = $PSBoundParameters.MaxSize
                $MaxFiles = $PSBoundParameters.MaxFiles

                $PathExist = Test-Path $BasePath        
                if (!$PathExist){
                    $tmp = New-Item -Path $BasePath -ItemType Directory
                }
        
                if (!$BasePath.EndsWith("\")){
                    $BasePath = $BasePath + "\"
                }
        
                $UserPath = $BasePath + $LogName
                if (!$UserPath.EndsWith("\")){
                    $UserPath = $UserPath + "\"
                }
                        
                $USerPathExist = Test-Path $UserPath
                if (!$USerPathExist){
                    $tmp = New-Item -Path $UserPath -ItemType Directory
                }
                
                $ergObject = [LMFile]::New($Name, $UserPath, $MaxSize);
                if ($FileRotation){
                    $ergObject.EnableFileRotation($MaxFiles);
                }
            }

            "Verbose"{
                $ergObject = [LMVerbose]::New($Name);
            }

            "Memory"{
                $ergObject = [LMMemory]::New($Name);
            }

            "EventLog"{
                $LogName = $PSBoundParameters.LogName;
                $Source = $PSBoundParameters.Source;
                $EventID = $PSBoundParameters.EventID;
                $RemoteHost = $PSBoundParameters.ComputerName;
                $ergObject = [LMEventLog]::New($Name, $LogName, $Source, $EventID);
                if ($RemoteHost -ne ""){
                    $ergObject.SetRemoteHost($RemoteHost);
                }
            }
        }
        
    }
    end{
        $ergObject
    }
}

Export-ModuleMember -Function *-*